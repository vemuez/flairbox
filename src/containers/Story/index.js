import React, { useMemo, useState } from "react";
import { fromJS } from "immutable";
import styled from "styled-components";

import StoryBlock from "../../components/StoryBlock";
import EmptyBlock from "../../components/EmptyBlock";
import Button from "../../components/Button";

export default function Story() {
  const max = 4;

  const [path, setPath] = useState([]);

  const [storyBlocks, setStoryBlocks] = useState(
    fromJS({ value: "", options: [] })
  );

  const curBlock = useMemo(() => {
    let block = storyBlocks.getIn(path);

    // TODO: Would be better to optimise this if had time
    if (!block) {
      block = storyBlocks.getIn([...path.slice(0, -2)]);
    }

    return block.toJS();
  }, [path, storyBlocks]);

  function handleClick(index) {
    setPath([...path, "options", index]);
  }

  function handleCreate() {
    setStoryBlocks(
      storyBlocks.updateIn([...path, "options"], (storyBlock) =>
        storyBlock.push(fromJS({ value: "", options: [] }))
      )
    );
  }

  function handleSave(value, index) {
    if (index === undefined) {
      return setStoryBlocks(
        storyBlocks.updateIn(path, (storyBlock) =>
          storyBlock.set("value", value)
        )
      );
    }

    setStoryBlocks(
      storyBlocks.updateIn([...path, "options", index], (storyBlock) =>
        storyBlock.set("value", value)
      )
    );
  }

  function handleDelete(index) {
    if (index === undefined && !path.length) {
      return;
    }

    if (index === undefined && path.length) {
      // TODO: Would be better to optimise this if had time
      setPath([...path.slice(0, -2)]);
      return setStoryBlocks(storyBlocks.deleteIn(path));
    }

    return setStoryBlocks(storyBlocks.deleteIn([...path, "options", index]));
  }

  return (
    <StoryCont className="App">
      {path.length > 0 && (
        <StoryActions>
          <Button onClick={() => setPath([...path.slice(0, -2)])}>&lt; Back</Button>
          <Button onClick={() => setPath([])}>🏠 Back to start</Button>
        </StoryActions>
      )}

      <StoryParent>
        <StoryBlock
          isAbsoluteParent={!path.length}
          value={curBlock.value}
          onSave={handleSave}
          onDelete={handleDelete}
        />
      </StoryParent>

      <StoryChildren>
        {curBlock.options.map((option, index) => (
          <StoryBlock
            key={index}
            value={option.value}
            onClick={() => handleClick(index)}
            onDelete={() => handleDelete(index)}
            onSave={(value) => handleSave(value, index)}
          />
        ))}

        {curBlock.options.length < max && <EmptyBlock onClick={handleCreate} />}
      </StoryChildren>
    </StoryCont>
  );
}

const StoryCont = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const StoryActions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;

  > * {
    &:not(:last-child) {
      margin-right: 1rem;
    }
  }
`

const StoryParent = styled.div`
  margin-bottom: 0.5rem;
`;

const StoryChildren = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`;
