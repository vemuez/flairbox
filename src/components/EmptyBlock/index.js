import React from "react";
import styled from "styled-components";

import Block from "../Block";

export default function EmptyBlock(props) {
  return <EmptyBlockCont {...props}>+ Create New</EmptyBlockCont>;
}

const EmptyBlockCont = styled(Block)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  color: ${({ theme }) => theme.primary};
  border: 4px dotted ${({ theme }) => theme.primary};

  &:hover {
    cursor: pointer;
    opacity: 0.6;
  }
`;
