import React from "react";
import styled from "styled-components";

export default function Error(props) {
  return <ErrorCont {...props} />;
}

const ErrorCont = styled.p`
  color: red;
  font-size: .75rem;
`;
