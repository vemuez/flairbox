import React from "react";
import classnames from "classnames";
import styled from "styled-components";

import Error from "../Error";

export default function Input({ error, ...props }) {
  return (
    <>
      <InputCont {...props} className={classnames({ error })} />
      {error && <Error>{error}</Error>}
    </>
  );
}

const InputCont = styled.input`
  width: 100%;
  display: inline-flex;

  &.error {
    border-color: red;
  }
`;
