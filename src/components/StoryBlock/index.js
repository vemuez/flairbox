import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";

import Block from "../Block";
import Button from "../Button";
import Error from "../Error";
import Input from "../Input";

export default function StoryBlock({
  value = "",
  isAbsoluteParent,
  onClick,
  onSave,
  onDelete
}) {
  const [edit, setEdit] = useState(!!value);
  const [error, setError] = useState("");
  const [curValue, setCurValue] = useState(value);

  useEffect(() => {
    setEdit(!value);
    setCurValue(value);
  }, [value]);

  useEffect(() => {
    if (curValue && error) {
      setError(false);
    }
  }, [curValue, error]);

  function handleSave() {
    if (!curValue.trim()) {
      return setError("Field cannot be empty");
    }

    onSave(curValue);
  }

  function handleCancel() {
    if (!value.trim()) {
      return onDelete();
    }

    setCurValue(value);
    setEdit(false);
  }

  if (edit) {
    return (
      <StoryBlockCont>
        <StoryBlockBody>
          <Input
            error={error}
            value={curValue}
            onChange={(e) => setCurValue(e.target.value)}
          />
        </StoryBlockBody>
        <StoryBlockActions>
          <Button disabled={curValue === value} onClick={handleSave}>
            Save
          </Button>
          <Button disabled={isAbsoluteParent && !value} onClick={handleCancel}>
            Cancel
          </Button>
          {!isAbsoluteParent && value && (
            <Button onClick={() => onDelete()}>Delete</Button>
          )}
        </StoryBlockActions>
      </StoryBlockCont>
    );
  }

  return (
    <StoryBlockCont onClick={onClick}>
      <StoryBlockBody>
        <p>{curValue}</p>
      </StoryBlockBody>
      <StoryBlockActions>
        <Button onClick={(e) => (setEdit(true), e.stopPropagation())}>
          Edit
        </Button>
      </StoryBlockActions>
    </StoryBlockCont>
  );
}

const StoryBlockCont = styled(Block)`
  display: flex;
  border: 0;    
  padding: 1rem;
  background: white;
  flex-direction: column;
  justify-content: space-between;
  color: ${({ theme }) => theme.text};

  ${({ onClick }) =>
    onClick &&
    css`
      border: 2px solid ${({ theme }) => theme.primary};

      &:hover {
        cursor: pointer;
        border-width: 4px;
      }
    `}
`;

const StoryBlockBody = styled.div`
  flex: 0 1 auto;
  display: flex;

  > p {
    margin: 0;
  }
`;

const StoryBlockActions = styled.div`
  flex: 0 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: center;

  > * {
    flex: 1 1 33%;

    &:not(:last-child) {
      margin-right: 0.5rem;
    }
  }
`;
