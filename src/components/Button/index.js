import React from "react";
import styled from "styled-components";

export default function Button(props) {
  return <ButtonCont {...props} />;
}

const ButtonCont = styled.button`
  border: none;
  padding: 1rem 0.5rem;
  border-radius: 0.15rem;
  background: ${({ theme }) => theme.primary};
  color: ${({ theme }) => theme.secondaryText};

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;
