import React from "react";
import styled from "styled-components";

export default function Block(props) {
  return (
    <BlockWrap>
      <BlockCont {...props} tabIndex="0" />
    </BlockWrap>
  );
}

const BlockWrap = styled.div`
  padding: 1rem;
`;

const BlockCont = styled.a`
  width: 225px;
  height: 225px;
  margin: -0.5rem;
  border-radius: 0.75rem;
  box-sizing: border-box;
`;
