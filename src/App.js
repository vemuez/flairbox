import React from "react";

import Story from "./containers/Story";

import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <Story />
    </div>
  );
}
